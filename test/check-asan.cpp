#include <iostream>

int main() {
  auto s = new char[100];
  s[0] = '\0';
  std::cout << s << std::endl;
#ifndef WITH_MEMORY_LEAK
  delete[] s;
#endif
  return 0;
}
