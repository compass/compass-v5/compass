from compass.glab import _version_string_as_tuple as version_as_tuple


def test_glab_version_tool():
    assert version_as_tuple("1") == (1, 0, 0)
    assert version_as_tuple("1.2") == (1, 2, 0)
    assert version_as_tuple("1.2.3") == (1, 2, 3)
    assert version_as_tuple("1.2.3") > version_as_tuple("1")
