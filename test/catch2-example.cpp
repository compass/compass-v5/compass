#include <exception>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Catch2 truism") { REQUIRE(true); }

void oops() { throw std::runtime_error{"Oops I did it wrong."}; }

TEST_CASE("Catch2 catching exceptions") {
  CHECK_THROWS_AS(oops(), std::runtime_error);
}
