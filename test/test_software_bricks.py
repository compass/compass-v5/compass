from compass.bricks import SoftwareBrick


def test_software_bricks():
    name = "ComPASS-v0"
    brick = SoftwareBrick("ComPASS-v0")
    assert brick.name == name
    assert brick.identifier == "compass_v0"
    assert brick.slug == "compass-v0"
