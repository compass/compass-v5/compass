# The ComPASS toolkit

This project is mainly a utility package that allows you to
install/use/develop the different software bricks that constitute
the ComPASS family of tools.

Once installed its adds the `compass` command.

Have a look at the .gitlab-ci.yml file that drives CI to have a look
at all the steps. If you do not use the `--repo ${COMPASS_GROUP_BASE_URL}`
argument it will default to https://gitlab.com/compass/compass-v5.

## Quick start

1. Run `compass -h` to display help or `compass <command> -h` do display help for a specific command.

1. You probably want to set a root directory that will contain all compass modules.
This is done with the `set-root`, `get-root`, `clear-root` command.

1. Once this is set-up you may want to clone modules. Check `compass clone`.

1. If you want to build modules use `compass build` or `compass superbuild`
(might be deprecated in favor of `compass build --all`).

1. To run pytest use `compass pytest` (e.g. `compass pytest --all`).

1. To check what is the module status: `compass status`

1. To run a git command in all modules directories sequentially: `compass git` (e.g. `compass git -- log -1`).

1. To run a (current) shell command in all modules directories sequentially: `compass shell`
(e.g. `compass shell pwd` or `compass shell -- ls -a`).

# gitlab integration

`bump-version` and `spread-dependencies` subcommands rely on
[gitlab CLI tool](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli)

It is included in compass sdk from version 5.0. You can also directly
install it from conda-forge
(`conda install -c conda-forge glab`).

## Configure glab

You must provide a way to authenticate to your gitlab instance.

1 - Create a personal access token with api and write_repo
on your gitlab instance ([link for bgrm instance](https://gitlab.brgm.fr/-/profile/personal_access_tokens?scopes=api,write_repository))
2 - Store this token in a secure file on your account (e.g. `~/.gitlab-token`)
3 - Add a line a the end of your `.bashrc` (use your actual gitlab instance):

```shell
glab auth login -h gitlab.brgm.fr --stdin < ~/.gitlab-token
```

NB: You will also need the `tomli`  and `tomli-w` packages that can be installed from pip.

# Writing tests

## Writing C++ tests with Catch2

We recommend to use the [Catch2](https://github.com/catchorg/Catch2) framework for testing C++ code.

Tests source file can just be added with the `compass_add_cpp_catch2tests`.

Tests will be automatically added to the list of tests to be run by CTest.
They can also be run independently with the `catch2tests` target, *i.e.* supposing
that you built the tests in the `build` directory you can run `build/catch2tests`.

A (terribly) simple example can be found in [test/catch2-example.cpp](test/catch2-example.cpp).

A tutorial on how to write Catch2 tests can be found
[here](https://github.com/catchorg/Catch2/blob/devel/docs/tutorial.md).

# Documentation generation

The `compass doc` command can be used to generate documentation in
the `<brick>/doc/html` folder.

Use `compass doc --help` to see available options.

After generation you can open the local `<brick>/doc/html/index.html` file in your favorite
web browser.
