find_package(Python COMPONENTS Interpreter Development)
find_package(nanobind)

# build the nanobind library shared version
nanobind_build_library(nanobind)

# nanobind sets the EXCLUDE_FROM_ALL on the built library the following is a
# trick to trigger the building of nanobind target
add_custom_target(_nanobind ALL)
add_dependencies(_nanobind nanobind)

install(TARGETS nanobind DESTINATION ${MODULE_DESTINATION_DIRECTORY})
