from pathlib import Path
import platform


def parent_dir(as_posix=False):
    path = Path(__file__).parent
    if as_posix:
        return path.as_posix()
    return path


def compass_rootfilepath():
    return parent_dir() / f".compass.root.{platform.system()}"


def get_compass_root():
    rootfilepath = compass_rootfilepath()
    root = None
    if rootfilepath.is_file():
        with rootfilepath.open() as f:
            root = Path(f.readline().strip())
            assert root.is_dir()
    return root
