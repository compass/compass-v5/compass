import tomli
import tomli_w
import re
import subprocess
import shlex
import time
import sys
from pathlib import Path


def _version_string_as_tuple(s):
    s = s.strip()
    m = re.match(r"(\d+)$", s)
    if m:
        return int(m.group(1)), 0, 0
    m = re.match(r"(\d+)\.(\d+)$", s)
    if m:
        return int(m.group(1)), int(m.group(2)), 0
    m = re.match(r"(\d+)\.(\d+)\.(\d+)$", s)
    if not m:
        raise ValueError(f"could not read version from {s}")
    return int(m.group(1)), int(m.group(2)), int(m.group(3))


class GitLabInteractor:
    def __init__(self, cwd=None, debug=False):
        self.cwd = Path()
        self.debug = debug
        if cwd is not None:
            self.cwd = Path(cwd)
            assert self.cwd.is_dir()

    def run(self, cmd, show=False, **kwargs):
        if show or self.debug:
            print(f"\n>> {cmd}")
        kwargs["cwd"] = self.cwd
        if not self.debug:
            kwargs["capture_output"] = True
        return subprocess.run(shlex.split(cmd), **kwargs)

    def crun(self, cmd, **kwargs):
        kwargs["check"] = True
        return self.run(cmd, **kwargs)

    def project_name(self):

        pyproject_file = self.cwd / "pyproject.toml"
        assert pyproject_file.is_file()

        with pyproject_file.open("rb") as f:
            try:
                conf = tomli.load(f)
            except Exception as e:
                print(f"Error reading {pyproject_file.as_posix()}")
                raise e
            return conf["project"]["name"]

    def upgrade_versions(self, versions, dry_run=False):

        pyproject_file = self.cwd / "pyproject.toml"
        assert pyproject_file.is_file()

        with pyproject_file.open("rb") as f:
            try:
                conf = tomli.load(f)
            except Exception as e:
                print(f"Error reading {pyproject_file.as_posix()}")
                raise e

        bumped = set()

        for section, subsection in [
            ("build-system", "requires"),
            ("project", "dependencies"),
        ]:
            dependencies = conf[section][subsection]
            for k, s in reversed(list(enumerate(dependencies))):
                for name in versions.keys():
                    if s.startswith(name):
                        version = re.match(rf"{name}>=(.+)", s).group(1)
                        version_t = _version_string_as_tuple(version)
                        new_version = versions[name][1:]
                        new_version_t = _version_string_as_tuple(new_version)
                        if new_version_t < version_t:
                            raise ValueError(
                                f"new version ({new_version}) should be greater than existing constraint ({version})!"
                            )
                        if new_version_t > version_t:
                            del dependencies[k]
                            bumped.add(name)
            for name in bumped:
                version = versions[name]
                assert version[0] == "v"
                dependencies.append(f"{name}>={version[1:]}")

        if not dry_run:
            with pyproject_file.open("wb") as f:
                tomli_w.dump(conf, f)
            self.run("pre-commit run --all")

        return bumped

    def wait_for_pipeline(self, name, branch):
        message = f"\n{self.project_name()}: wait for {name} pipeline to succeed"
        if name.startswith("!"):
            message += f" (branch {branch})"
        print(message)
        status = ""
        retry = True  # sometimes the tag pipeline is not created fast enough
        while status != "success":
            ret = self.run(f"glab ci get -b {branch}", capture_output=True, show=False)
            if ret.returncode != 0:
                if retry:
                    retry = False
                    time.sleep(1)
                    continue
                else:
                    print(f"Something went wrong with command: glab ci get -b {branch}")
                    for line in ret.stderr.split(b"\n"):
                        print(line.decode().strip())
                    raise RuntimeError("Could not fetch pipeline.")
            output = ret.stdout
            while output:
                line, _, output = output.partition(b"\n")
                m_status = re.match(r"status:\s*(\w+)", line.strip().decode())
                if m_status is not None:
                    status = m_status.group(1)
                    break
            if status != "success":
                if status == "failed":
                    print(f"\n!!! Pipeline FAILED !!!\n")
                    for line in output.split(b"\n"):
                        print(line.decode().strip())
                    sys.exit(-1)
                time.sleep(1)

    def branch_sha1(self, name):
        ret = self.crun(f"git rev-parse {name}", capture_output=True)
        return ret.stdout.decode().strip()

    def current_branch(self):
        ret = self.crun("git rev-parse --abbrev-ref HEAD", capture_output=True)
        return ret.stdout.decode().strip()

    def _check_main_sync(self):
        self.crun(f"git checkout main")
        self.crun(f"git fetch origin")
        if self.branch_sha1("main") != self.branch_sha1("origin/main"):
            print(
                f"\n{self.project_name()}: main and origin/main must be on the same sha1"
            )
            sys.exit(-1)

    def latest_tag(self):
        self.crun("git fetch origin --prune")
        ret = self.run(
            "git describe --tags --abbrev=0", show=False, capture_output=True
        )
        if ret.returncode != 0:
            raise RuntimeError(f"No tags found for {self.project_name()}!")
        return ret.stdout.decode().strip()

    def current_tag(self):
        self._check_main_sync()
        ret = self.run(
            "git describe --exact-match --tags HEAD", show=False, capture_output=True
        )
        if ret.returncode != 0:
            raise RuntimeError("HEAD is not on a tag!")
        return ret.stdout.decode().strip()

    def tag_main(self, tag, annotation=None, dry_run=False, advance=False):
        ret = self.run(
            "git describe --exact-match --tags HEAD", show=False, capture_output=True
        )
        if ret.returncode == 0 and not (dry_run and advance):
            print("\nHEAD has already one tag!")
            sys.exit(-1)
        if dry_run:
            print(f"{self.project_name()} would be tagged with {tag}")
            return
        self._check_main_sync()
        if annotation:
            self.crun(f'git tag {tag} -m "{annotation}"')
        else:
            self.crun(f"git tag {tag}")
        self.crun(f"git push origin {tag}")
        self.wait_for_pipeline(f"{tag} tag", tag)
        print("We wait 10s for registry to synchronize.")
        time.sleep(10)

    def next_tag(self, component="patch"):

        assert component in ["patch", "minor", "major"]

        ret = self.crun(
            f"git describe --tags --abbrev=0", capture_output=True, show=False
        )
        latest_tag = ret.stdout.decode().strip()
        assert latest_tag[0] == "v"
        maj, min, patch = _version_string_as_tuple(latest_tag[1:])
        if component == "patch":
            return f"v{maj}.{min}.{patch+1}"
        elif component == "minor":
            return f"v{maj}.{min+1}.0"
        assert component == "major"
        return f"v{maj+1}.0.0"

    def merge_into_main(self, source_branch=None):
        if source_branch is None:
            source_branch = self.current_branch()
        self.crun(f"glab mr new -f -y --remove-source-branch")
        self.crun(f"git checkout main")
        self.crun(f"git branch -D {source_branch}")
        ret = self.crun(f"glab mr ls -s {source_branch}", capture_output=True)
        for line in ret.stdout.split(b"\n"):
            if line.startswith(b"!"):
                mr_id = int(line.split()[0].strip()[1:])
                break
        self.wait_for_pipeline(f"!{mr_id}", source_branch)
        self.crun(f"glab mr merge {mr_id} -d -y")
        self.crun(f"git pull --prune --ff-only")

    def bump_dependencies(
        self,
        versions,
        dry_run=False,
        local=False,
        tag_changes=False,
        push=False,
        component="patch",
    ):

        if not local:
            self.crun(f"git checkout main")
            self.crun(f"git pull --prune --ff-only")

        bumped = self.upgrade_versions(versions, dry_run=dry_run)

        if len(bumped) == 0:
            print(f"No changes for {self.project_name()}")
            return None
        if dry_run:
            if bumped:
                print(f"Changes for {self.project_name()}:")
                for name in bumped:
                    print(f"- {name}>={versions[name][1:]}")

        if not dry_run and (push or tag_changes):
            slug = "-".join([versions[name] for name in bumped])
            source_branch = f"auto/bump-{'-'.join(bumped)}_{slug}"
            self.crun(f"git checkout -b {source_branch}")
            self.run(f"pre-commit run --file pyproject.toml")
            self.crun(f"git add pyproject.toml")
            message = f'Bump {" ".join(bumped)} version\n\n'
            for name in bumped:
                message += f"- {name}>={versions[name][1:]}\n"
            self.crun(f"git commit -m '{message}'")
            self.crun(f"git push -u origin HEAD")

        if tag_changes:
            if not dry_run:
                self.merge_into_main(source_branch)
            new_tag = self.next_tag(component)
            self.tag_main(new_tag, dry_run=dry_run, advance=True)
            return new_tag
