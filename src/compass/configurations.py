import yaml
from pathlib import Path

from compass.directories import parent_dir


def _confpath(s):
    return parent_dir() / f".compass.configurations.{s}"


class Configuration:
    def __init__(self, path):
        self._confs = {}
        self.path = path
        if path.is_file():
            with path.open("r") as f:
                self._confs = yaml.safe_load(f) or {}

    def __len__(self):
        return len(self._confs)

    def __getitem__(self, key):
        return self._confs[key]

    def __delitem__(self, key):
        del self._confs[key]

    def __setitem__(self, key, value):
        self._confs[key] = value

    def __contains__(self, key):
        return key in self._confs

    def items(self):
        return self._confs.items()

    def names(self):
        return self._confs.keys()

    def save(self):
        with self.path.open("w") as f:
            yaml.safe_dump(self._confs, f)

    def isdisjoint(self, other):
        if not isinstance(other, Configuration):
            return True
        return set(self._confs.keys()).isdisjoint(set(other._confs.keys()))


def collect_configurations():
    versioned = Configuration(_confpath("versioned"))
    local = Configuration(_confpath("local"))
    assert versioned.isdisjoint(local)
    return versioned, local
