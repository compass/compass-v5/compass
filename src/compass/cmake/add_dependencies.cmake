# target name -> tname
macro(compass_add_dependencies tname)

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs PACKAGES)
  cmake_parse_arguments(arg "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  set(${PROJECT_NAME}_DEPENDENCIES
      ""
      CACHE STRING "project dependencies")

  foreach(dep ${arg_PACKAGES})
    if(${dep} IN_LIST ${PROJECT_NAME}_DEPENDENCIES)
      # we do not add several time the same package dependency
      continue()
    endif()
    find_package(${dep})
    list(APPEND ${PROJECT_NAME}_DEPENDENCIES ${dep})
  endforeach()

  set(${PROJECT_NAME}_DEPENDENCIES
      ${${PROJECT_NAME}_DEPENDENCIES}
      CACHE STRING "project dependencies" FORCE)

endmacro()
