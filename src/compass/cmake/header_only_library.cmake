# Header Only Target -> hot
macro(compass_header_only_library hot_NAME)

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
  cmake_parse_arguments(hot "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  add_library(${hot_NAME} INTERFACE)

  foreach(_path ${hot_FILES})
    list(APPEND hot_HEADERS ${PROJECT_PYTHON_MODULE_DIR}/${_path})
  endforeach()

  string(MAKE_C_IDENTIFIER ${hot_NAME} hot_C_VALID_NAME)
  set(hot_fileset ${hot_C_VALID_NAME}_headers)

  target_sources(${hot_NAME} INTERFACE FILE_SET ${hot_fileset} TYPE HEADERS
                                       FILES ${hot_HEADERS})

  target_compile_features(${hot_NAME} INTERFACE ${COMPASS_CXX_STD_FEATURE})
  if(COMPASS_HAS_NO_DEPRECATED_CODE)
    target_compile_definitions(${hot_NAME}
                               INTERFACE COMPASS_HAS_NO_DEPRECATED_CODE)
  endif()
  set_target_properties(
    ${hot_NAME} PROPERTIES CXX_STANDARD_REQUIRED ${COMPASS_REQUIRE_CXX_STANDARD}
                           EXPORT_PROPERTIES "CXX_STANDARD_REQUIRED")

  foreach(visibility PUBLIC PRIVATE INTERFACE)
    if(hot_${visibility})
      target_link_libraries(${hot_NAME} ${visibility} ${hot_${visibility}})
    endif()
  endforeach()

  install(
    TARGETS ${hot_NAME}
    EXPORT ${PROJECT_NAME}-targets
    FILE_SET ${hot_fileset})

endmacro()
