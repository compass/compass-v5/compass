macro(compass_add_site_packages_to_prefix_path)

  execute_process(
    COMMAND compass site-packages-dir
    OUTPUT_VARIABLE SITE_PACKAGES_DIR
    OUTPUT_STRIP_TRAILING_WHITESPACE)
  list(PREPEND CMAKE_PREFIX_PATH ${SITE_PACKAGES_DIR})

endmacro()
