include(CMakePackageConfigHelpers)

macro(compass_export_project_targets)

  if(${PROJECT_NAME}_DEPENDENCIES)
    foreach(dep ${${PROJECT_NAME}_DEPENDENCIES})
      string(APPEND PROJECT_DEPENDENCIES "find_dependency(${dep})\n")
    endforeach()
  endif()

  configure_package_config_file(
    ${COMPASS_CMAKE_DIR}/config.cmake.in
    ${SKBUILD_PLATLIB_DIR}/${PROJECT_NAME}-config.cmake INSTALL_DESTINATION ".")

  write_basic_package_version_file(
    ${SKBUILD_PLATLIB_DIR}/${PROJECT_NAME}-version.cmake
    VERSION ${SKBUILD_PROJECT_VERSION}
    COMPATIBILITY AnyNewerVersion)

  install(
    EXPORT ${PROJECT_NAME}-targets
    FILE ${PROJECT_NAME}-targets.cmake
    # TODO: NAMESPACE ComPASS:: ?
    DESTINATION ${SKBUILD_PLATLIB_DIR})

endmacro()
