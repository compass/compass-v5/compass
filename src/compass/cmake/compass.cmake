include_guard(GLOBAL)

# NB: the order of inclusion matters
include(configure_build)
include(add_dependencies)
include(header_only_library)
include(shared_library)
include(add_nb_module)
include(export_project_targets)
include(add_cpp_tests)
include(add_mpi_tests)
include(add_cpp_doctests)
include(add_cpp_catch2tests)
include(with_address_sanitizer)
