option(COMPASS_USES_MPI "enable MPI accross ComPASS modules that can use it" ON)

# Overrides compilation flags for accross all compass modules We force -O0 in
# DEBUG mode so that asan works
option(COMPASS_NO_COMPILATION_WARNINGS
       "do not issue warnings during compilation" OFF)
option(COMPASS_HAS_NO_DEPRECATED_CODE "remove deprecations from build" OFF)
option(COMPASS_IGNORE_DEPRECATIONS
       "ignore deprecation warnings during compilation" OFF)
if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g3 -fdiagnostics-color=always")
  if(COMPASS_NO_COMPILATION_WARNINGS)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -w")
  else()
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra")
    if(COMPASS_IGNORE_DEPRECATIONS)
      set(CMAKE_CXX_FLAGS_DEBUG
          "${CMAKE_CXX_FLAGS_DEBUG} -Wno-deprecated-declarations")
    endif()
  endif()
endif()

macro(compass_configure_build)

  if(NOT DEFINED SKBUILD)
    message(
      FATAL_ERROR "compilation is supposed to be driven by scikit-build-core")
  endif()
  if(${SKBUILD} LESS 2)
    message(
      FATAL_ERROR "compilation is supposed to be driven by scikit-build-core")
  endif()
  if(${SKBUILD_CORE_VERSION} VERSION_LESS 0.5.1)
    message(FATAL_ERROR "scikit-build-core version should be at least 0.5.1")
  endif()

  if(${CMAKE_BINARY_DIR} PATH_EQUAL ${PROJECT_SOURCE_DIR})
    message(
      FATAL_ERROR
        "

  In-source compilation forbidden.
  Build a directory build: mkdir build.
  Then: cd build; cmake ..

")
  endif()

  if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE
        "Release"
        CACHE
          STRING
          "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel."
          FORCE)
  endif()

  set(COMPASS_CXX_STD_FEATURE "cxx_std_23")
  option(COMPASS_REQUIRE_CXX_STANDARD
         "Enforce C++ standard for ComPASS targets" ON)

  option(COMPASS_USES_SSCACHE OFF)

  if(COMPASS_USES_SSCACHE)
    find_program(SCCACHE sccache REQUIRED)
    set(CMAKE_C_COMPILER_LAUNCHER ${SCCACHE})
    set(CMAKE_CXX_COMPILER_LAUNCHER ${SCCACHE})
    set(CMAKE_MSVC_DEBUG_INFORMATION_FORMAT Embedded)
    cmake_policy(SET CMP0141 NEW)
  endif()

  # option(BUILD_TESTING "Build test suite" ON)

  # option(BUILD_DOC "Build documentation" OFF)

  string(MAKE_C_IDENTIFIER ${PROJECT_NAME} PROJECT_PYTHON_MODULE_DIR)
  # install prefixes must be set before declaring target export this include all
  # tagets defined in sub directories don't forget dependencies
  if(SKBUILD_STATE STREQUAL "editable")
    set(CMAKE_INSTALL_PREFIX ${CMAKE_CURRENT_SOURCE_DIR}/src)
    set(MODULE_DESTINATION_DIRECTORY
        "${CMAKE_CURRENT_SOURCE_DIR}/src/${PROJECT_PYTHON_MODULE_DIR}")
  elseif(SKBUILD_STATE STREQUAL "wheel")
    set(CMAKE_INSTALL_PREFIX "\${CMAKE_CURRENT_LIST_DIR}")
    set(MODULE_DESTINATION_DIRECTORY "${PROJECT_PYTHON_MODULE_DIR}")
  else()
    message(FATAL_ERROR "Unknown build state: ${SKBUILD_STATE}")
  endif()
  set(CMAKE_INSTALL_INCLUDEDIR ".")

  # clear dependencies
  set(${PROJECT_NAME}_DEPENDENCIES
      ""
      CACHE STRING "project dependencies" FORCE)

endmacro()
