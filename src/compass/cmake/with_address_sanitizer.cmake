macro(compass_with_address_sanitizer SANITIZED_TARGET)

  if(NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
    message(
      FATAL_ERROR "Build type MUST be in Debug mode to use address sanitizer.")
  endif()

  if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
    target_compile_options(${SANITIZED_TARGET} PRIVATE -fsanitize=address)
    target_link_options(${SANITIZED_TARGET} PRIVATE -fsanitize=address)
  elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "MSVC")
    target_compile_options(${SANITIZED_TARGET} PRIVATE /fsanitize=address)
  else()
    message(FATAL_ERROR "The compiler is not handled for Address Sanitizer")
  endif()

endmacro()
