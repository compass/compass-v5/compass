include(GenerateExportHeader)

# Shared Library Target -> slt
macro(compass_shared_library slt_NAME)

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
  cmake_parse_arguments(slt "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  set(slt_SOURCES)
  foreach(_path ${slt_FILES})
    list(APPEND slt_SOURCES sources/${_path})
  endforeach()

  add_library(${slt_NAME} SHARED ${slt_SOURCES})
  generate_export_header(${slt_NAME})
  target_include_directories(
    ${slt_NAME}
    PUBLIC
      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_PYTHON_MODULE_DIR}>
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
      $<INSTALL_INTERFACE:${PROJECT_PYTHON_MODULE_DIR}>
      $<INSTALL_INTERFACE:${PROJECT_PYTHON_MODULE_DIR}/${CMAKE_CXX_COMPILER_ID}>
  )

  target_compile_features(${slt_NAME} INTERFACE ${COMPASS_CXX_STD_FEATURE})
  set_target_properties(
    ${slt_NAME}
    PROPERTIES CXX_STANDARD_REQUIRED ${COMPASS_REQUIRE_CXX_STANDARD}
               EXPORT_PROPERTIES "CXX_STANDARD_REQUIRED"
               INSTALL_RPATH_USE_LINK_PATH TRUE)

  foreach(visibility PUBLIC PRIVATE INTERFACE)
    if(slt_${visibility})
      target_link_libraries(${slt_NAME} ${visibility} ${slt_${visibility}})
    endif()
  endforeach()

  install(
    TARGETS ${slt_NAME}
    EXPORT ${PROJECT_NAME}-targets
    DESTINATION ${MODULE_DESTINATION_DIRECTORY})

  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${slt_NAME}_export.h
          DESTINATION ${MODULE_DESTINATION_DIRECTORY}/${CMAKE_CXX_COMPILER_ID})

endmacro()
