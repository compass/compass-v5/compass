# NanoBind Module -> nbm
macro(compass_add_nb_module nbm_NAME)

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
  cmake_parse_arguments(nbm "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  if(TARGET ${nbm_NAME})
    message(
      FATAL_ERROR "${nbm_NAME} is an already existing target in this project")
  endif()

  set(nbm_SOURCES)
  foreach(_path ${nbm_FILES})
    list(APPEND nbm_SOURCES bindings/${_path})
  endforeach()

  nanobind_add_module(${nbm_NAME} NB_SHARED ${nbm_SOURCES})

  target_compile_features(${nbm_NAME} INTERFACE ${COMPASS_CXX_STD_FEATURE})
  set_target_properties(
    ${nbm_NAME}
    PROPERTIES CXX_STANDARD_REQUIRED ${COMPASS_REQUIRE_CXX_STANDARD}
               EXPORT_PROPERTIES "CXX_STANDARD_REQUIRED"
               INSTALL_RPATH_USE_LINK_PATH TRUE)

  foreach(visibility PUBLIC PRIVATE INTERFACE)
    if(nbm_${visibility})
      target_link_libraries(${nbm_NAME} ${visibility} ${nbm_${visibility}})
    endif()
  endforeach()

  install(TARGETS ${nbm_NAME} DESTINATION ${MODULE_DESTINATION_DIRECTORY})

endmacro()
