include(CTest)

macro(compass_add_cpp_doctests)

  message(DEPRECATION "\ndoctest is deprecated in favor of Catch2\n"
                      "Use the compass_add_cpp_catch2tests macro instead.\n")

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
  cmake_parse_arguments(ct "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  execute_process(
    COMMAND compass doctest-dir
    OUTPUT_VARIABLE DOCTEST_DIR
    OUTPUT_STRIP_TRAILING_WHITESPACE)

  set(DOCTEST_CPP_MAIN ${CMAKE_CURRENT_BINARY_DIR}/doctest.cpp)
  file(COPY ${DOCTEST_DIR}/doctest.cpp DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

  set(target doctest)
  add_executable(${target} ${DOCTEST_CPP_MAIN} ${ct_FILES})
  target_include_directories(${target} PRIVATE ${DOCTEST_DIR})
  if(COMPASS_HAS_NO_DEPRECATED_CODE)
    target_compile_definitions(${target} PUBLIC COMPASS_HAS_NO_DEPRECATED_CODE)
  endif()
  foreach(visibility PUBLIC PRIVATE INTERFACE)
    if(ct_${visibility})
      target_link_libraries(${target} ${visibility} ${ct_${visibility}})
    endif()
  endforeach()

endmacro()
