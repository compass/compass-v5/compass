include_guard(GLOBAL)

execute_process(
  COMMAND compass cmake-config-dir --as-posix
  OUTPUT_VARIABLE COMPASS_CONFIG_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE)
list(PREPEND CMAKE_PREFIX_PATH ${COMPASS_CONFIG_DIR})

find_package(compass REQUIRED)
