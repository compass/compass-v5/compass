include(CTest)

macro(compass_add_cpp_tests)

  message(WARNING "\nPrefer to embed your tests in Catch2 framework "
                  "using the compass_add_cpp_catch2tests macro.\n")

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
  cmake_parse_arguments(ct "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  execute_process(
    COMMAND compass doctest-dir
    OUTPUT_VARIABLE DOCTEST_DIR
    OUTPUT_STRIP_TRAILING_WHITESPACE)

  foreach(f ${ct_FILES})

    get_filename_component(basename ${f} NAME_WLE)
    set(target test-${basename})
    add_executable(${target} ${f})
    add_test(NAME ${target} COMMAND ${target})

    target_include_directories(${target} PRIVATE ${DOCTEST_DIR})
    if(COMPASS_HAS_NO_DEPRECATED_CODE)
      target_compile_definitions(${target}
                                 PUBLIC COMPASS_HAS_NO_DEPRECATED_CODE)
    endif()

    foreach(visibility PUBLIC PRIVATE INTERFACE)
      if(ct_${visibility})
        target_link_libraries(${target} ${visibility} ${ct_${visibility}})
      endif()
    endforeach()

  endforeach()

endmacro()
