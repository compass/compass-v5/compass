include(CTest)

# https://github.com/catchorg/Catch2/blob/devel/docs/cmake-integration.md#top
find_package(Catch2 3 REQUIRED)
include(Catch)

macro(compass_add_cpp_catch2tests)

  set(options "")
  set(oneValueArgs "")
  set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
  cmake_parse_arguments(ct "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGV})

  set(target catch2tests)

  add_executable(${target} ${ct_FILES})
  if(COMPASS_HAS_NO_DEPRECATED_CODE)
    target_compile_definitions(${target} PUBLIC COMPASS_HAS_NO_DEPRECATED_CODE)
  endif()
  target_link_libraries(${target} PRIVATE Catch2::Catch2WithMain)
  foreach(visibility PUBLIC PRIVATE INTERFACE)
    if(ct_${visibility})
      target_link_libraries(${target} ${visibility} ${ct_${visibility}})
    endif()
  endforeach()
  catch_discover_tests(${target})

endmacro()
