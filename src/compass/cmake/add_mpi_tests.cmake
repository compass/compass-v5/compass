include(CTest)

set(COMPASS_MPI_NB_PROC
    4
    CACHE STRING "the number of procs to be used in tests that use MPI")

macro(compass_add_mpi_tests)

  if(NOT COMPASS_USES_MPI)

    message(
      WARNING
        "You must set COMPASS_USES_MPI to ON to actually add mpi dependent targets to the tests."
    )

  else()

    set(options "")
    set(oneValueArgs "")
    set(multiValueArgs FILES PUBLIC PRIVATE INTERFACE)
    cmake_parse_arguments(ct "${options}" "${oneValueArgs}" "${multiValueArgs}"
                          ${ARGV})

    set(MPIEXE_OPTIONS)
    if(UNIX)
      set(MPIEXE_OPTIONS "--allow-run-as-root")
    endif()

    foreach(f ${ct_FILES})

      get_filename_component(basename ${f} NAME_WLE)
      set(target test-${basename})
      add_executable(${target} ${f})
      add_test(NAME ${target}
               COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXE_OPTIONS} -n
                       ${COMPASS_MPI_NB_PROC} $<TARGET_FILE:${target}>)

      foreach(visibility PUBLIC PRIVATE INTERFACE)
        if(ct_${visibility})
          target_link_libraries(${target} ${visibility} ${ct_${visibility}})
        endif()
      endforeach()

    endforeach()

  endif()

endmacro()
