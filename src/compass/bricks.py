from pathlib import Path
from compass.directories import get_compass_root


class SoftwareBrick:
    def __init__(
        self,
        package_name,
        *,
        identifier=None,
        slug=None,
        pure_cpp=False,
        pure_python=False,
        subbuilds=None,
        has_python_tests=True,
        has_cpp_tests=True,
    ):
        self.name = package_name
        self._identifier = identifier
        self._slug = slug
        assert not (pure_cpp and pure_python)
        self.pure_cpp = pure_cpp
        self.pure_python = pure_python
        self.subbuilds = subbuilds or []
        self.has_python_tests = has_python_tests and not pure_cpp
        self.has_cpp_tests = has_cpp_tests and not pure_python

    @property
    def identifier(self):
        if self._identifier:
            return self._identifier
        return self.name.lower().replace("-", "_")

    @property
    def slug(self):
        if self._slug:
            return self._slug
        return self.name.lower()

    def __eq__(self, other):
        return self.name == other.name


# NB: order matters in the following list (cf. transitive dependencies)
all_bricks = [
    SoftwareBrick("compass-cxx-utils", pure_cpp=True),
    SoftwareBrick("compass-python-utils", identifier="compass_utils", pure_python=True),
    SoftwareBrick("nanobind-utils", has_cpp_tests=False, subbuilds=["test"]),
    SoftwareBrick("icus", subbuilds=["test/custom_field_example"]),
    SoftwareBrick("loaf"),
    SoftwareBrick("icmesh"),
    SoftwareBrick("pscal", pure_cpp=True),
    SoftwareBrick("scheme-lfv"),
    SoftwareBrick("scheme-vem"),
    SoftwareBrick("demo-scheme-vem"),
    SoftwareBrick("compass-physics", identifier="physics", slug="physics"),
    SoftwareBrick("physicalprop"),
    SoftwareBrick(
        "physical-properties-library", identifier="prop_library", pure_python=True
    ),
    SoftwareBrick("coats-variables"),
    SoftwareBrick("globalgo", pure_python=True),
    SoftwareBrick("compass-coats"),
]


def get_brick(name):
    for brick in all_bricks:
        if brick.name == name:
            return brick
    # try to find a directory
    path = Path(name)
    if path.is_dir():
        path = path.resolve()
        root = get_compass_root()
        if path != root and root in path.parents:
            candidate = path.relative_to(root).parts[0]
            for brick in all_bricks:
                if candidate in [brick.name, brick.identifier, brick.slug]:
                    return brick

    raise ValueError(f"No brick or valid directory named {name}")
