from pathlib import Path
import copy
import os
import sys
import contextlib
import site
import importlib
import platform
import shlex
import subprocess
import functools
import shutil
import stat
import re

import click

import verstr

try:
    from . import _version

    __version__ = verstr.verstr(_version.version)
except ImportError:
    __version__ = None


from compass.directories import *
from compass.configurations import collect_configurations
from compass.glab import GitLabInteractor

_versioned_configs, _local_configs = collect_configurations()


def _run(cmd, path=None, check=True, **kwargs):
    kwargs["cwd"] = path
    if not kwargs.get("capture_output", False):
        kwargs["stdout"] = sys.stdout
        kwargs["stderr"] = sys.stderr
    ret = subprocess.run(cmd, **kwargs)
    if check and ret.returncode != 0:
        sys.exit(ret.returncode)
    return ret


def ship_nanobind():
    libname = {"Windows": "nanobind.dll", "Linux": "libnanobind.so"}[platform.system()]
    return (parent_dir() / libname).is_file()


def icus():
    if platform.system() == "Windows":
        import icus

        return os.add_dll_directory(Path(icus.__file__).parent)
    return contextlib.nullcontext()


def nanobind():
    assert ship_nanobind, "nanobind shared library seems to be missing"
    if platform.system() == "Windows":
        return os.add_dll_directory(parent_dir())
    return contextlib.nullcontext()


def site_packages_dir(as_posix=False):
    path = Path(site.getsitepackages()[0]).resolve()
    if as_posix:
        return path.as_posix()
    return path


def resolve_filepath(path):
    """path is assumed to be relative to parent directory"""
    path = parent_dir() / Path(path)
    if not path.is_file():
        raise FileNotFoundError(f"Could not find file {path}")
    return path.resolve()


def resolve_dirpath(path):
    """path is assumed to be relative to parent directory"""
    path = parent_dir() / Path(path)
    if not path.is_dir():
        raise FileNotFoundError(f"Could not find directory {path}")
    return path.resolve()


@click.group(
    invoke_without_command=True,
    context_settings=dict(help_option_names=["-h", "--help"]),
)
@click.pass_context
def main(ctx):
    # TODO: --as-posix should be defined at this level
    if ctx.invoked_subcommand is None:
        # do something with the main command
        pass
    else:
        # do something before the subcommand
        pass


def old_main():
    print("", file=sys.stderr)
    print("WARNING !", file=sys.stderr)
    print(
        "WARNING ! This command is deprecated - prefer compass instead of compass-config",
        file=sys.stderr,
    )
    print("WARNING !", file=sys.stderr)
    print("", file=sys.stderr)
    sys.argv[0] = "compass"
    main()


@main.command("cmake-dir")
@click.option("--as-posix", is_flag=True)
def cmake_dir_cmd(as_posix):
    """print the directory containing specific cmake modules"""
    path = resolve_dirpath("cmake")
    if as_posix:
        path = path.as_posix()
    click.echo(path)


@main.command("cmake-config-dir")
@click.option("--as-posix", is_flag=True)
def cmake_config_dir_cmd(as_posix):
    """print the directory containing all cmake configuration files"""
    for path in (Path(s) for s in site.getsitepackages()):
        if (path / "compass-config.cmake").is_file():
            break
    else:
        print(
            f"ERROR: there is no compass-config file in any of the site packages",
            sys.stderr,
        )
        sys.exit(-1)
    if as_posix:
        path = path.as_posix()
    click.echo(path)


@main.command("use-compass-file")
@click.option("--as-posix", is_flag=True)
def use_compass_file_cmd(as_posix):
    """print the path to the use-compass.cmake file"""
    path = resolve_dirpath("cmake") / "use-compass.cmake"
    assert path.is_file()
    if as_posix:
        path = path.as_posix()
    click.echo(path)


@main.command("doctest-dir")
@click.option("--as-posix", is_flag=True)
def doctest_dir_cmd(as_posix):
    """print the directory containing the doctest header file"""
    path = resolve_dirpath("doctest")
    assert (path / "doctest.h").is_file()
    if as_posix:
        path = path.as_posix()
    click.echo(path)


@main.command("site-packages-dir", deprecated=True)
@click.option("--as-posix", is_flag=True)
def site_packages_dir_cmd(as_posix):
    """print the python site packages directory corresponding to compass"""
    click.echo(site_packages_dir(as_posix))


def _prepend_to_path(var, sep, as_posix=False):
    new_ldp = [f"{parent_dir(as_posix)}"]
    ldp = os.environ.get(var, "").split(sep)
    for path in ldp:
        if path and path not in new_ldp:
            if as_posix:
                path = path.as_posix()
            new_ldp.append(path)
    return sep.join(new_ldp)


def prepend_to_ld_path(as_posix=False):
    if platform.system() != "Linux":
        raise RuntimeError("Linux platform is required.")
    return _prepend_to_path("LD_LIBRARY_PATH", ":", as_posix)


def prepend_to_dll_path(as_posix=False):
    if platform.system() != "Windows":
        raise RuntimeError("Windows platform is required.")
    return _prepend_to_path("PATH", ";", as_posix)


@main.command("nanobind")
@click.option(
    "--dir",
    is_flag=True,
    help="print the directory containing the nanobind shared library",
)
@click.option(
    "--ld-path",
    is_flag=True,
    help="on Linux systems print the LD_LIBRARY_PATH envrionment variable prepending the nanobind shared library directory path",
)
@click.option(
    "--dll-path",
    is_flag=True,
    help="on Windows systems print the PATH envrionment variable prepending the nanobind shared library directory path",
)
@click.option("--as-posix", is_flag=False, help="use POSIX format")
def nanobind_cmd(dir, ld_path, dll_path, as_posix):
    """utilities to find nanobind shared library"""
    if dir and ld_path:
        raise RuntimeError("--dir and --ld-path are mutually exclusive")
    if not ship_nanobind():
        raise RuntimeError(
            f"Could not find the nanobind shared library in {parent_dir()}"
        )
    if dir:
        click.echo(parent_dir(as_posix))
    if ld_path:
        click.echo(prepend_to_ld_path(as_posix))
    if dll_path:
        click.echo(prepend_to_dll_path(as_posix))


@main.command("version")
@click.option(
    "--greater", "lower_bound", type=str, help="check version is greater than TEXT"
)
def version_cmd(lower_bound):
    """print compass version when called without options"""
    if lower_bound:
        if __version__ < lower_bound:
            raise RuntimeError(
                f"Found version {__version__} which is lower than {lower_bound}"
            )
    else:
        click.echo(f"{__version__}")


@main.command("set-root")
@click.argument("directory", type=click.Path(exists=True), nargs=-1)
def set_root_cmd(directory):
    """set the compass root directory that will contain all modules"""
    if len(directory) > 1:
        raise RuntimeError("Only one directory path is allowed.")
    new_root = Path.cwd() if len(directory) == 0 else Path(directory[0])
    new_root = new_root.resolve()
    if not new_root.is_dir():
        raise RuntimeError(f"{new_root} does not seem to be an existing directory")
    root = get_compass_root()
    if root and root == new_root:
        click.echo(f"ComPASS root is left unchanged at: {root}")
        return
    if root:
        click.echo("WARNING: changing ComPASS root!")
        click.echo(f"replacing: {root}")
        click.echo(f"with: {new_root}")
    with compass_rootfilepath().open("w") as f:
        print(new_root, file=f)


@main.command("get-root")
@click.option("--as-posix", is_flag=True)
def get_root_cmd(as_posix):
    """get the compass root directory that will contain all modules"""
    root = get_compass_root()
    if root:
        root = root.resolve()
        if as_posix:
            click.echo(root.as_posix())
        click.echo(root)
    else:
        click.echo("NO root directory")


@main.command("clear-root")
def clear_root_cmd():
    """clear the compass root directory (the directory content is left unchanged)"""
    compass_rootfilepath().unlink(missing_ok=False)


@main.command("status")
def status_cmd():
    """list all installed software bricks with their version and location"""

    from compass.bricks import all_bricks as bricks

    click.echo(f"compass {__version__} at {parent_dir().resolve()}")
    root = get_compass_root()
    if root:
        click.echo(f"compass-root set at: {root}")
    click.echo("-" * 80)
    for brick in bricks:
        try:
            module = importlib.import_module(brick.identifier)
        except ModuleNotFoundError:
            click.echo(f"ERROR: could not find module: {brick.name}")
            continue
        mess = f" - {brick.name:30}"
        if hasattr(module, "__version__"):
            mess += f"v{str(module.__version__):30}"
        else:
            mess += f"{'NO VERSION':30}"
        if module.__file__ is None:
            mess += f" WARNING no {brick.identifier}.__init__.py"
        else:
            module_path = Path(module.__file__).parent
            if root:
                if root in module_path.parents:
                    mess += f" LOCAL  at *compass-root*/{module_path.relative_to(root)}"
                else:
                    mess += f" SYSTEM at {module_path}"
            else:
                mess += f" at {module_path}"
        click.echo(mess)


def _build_banner(name):
    click.echo("")
    click.echo(f"=" * 80)
    click.echo(f"== ")
    click.echo(f"== {name} build")
    click.echo(f"== ")
    click.echo(f"=" * 80)


def _brick_path(brick):
    root = get_compass_root()
    if not root:
        raise RuntimeError("You must set a root first!")
    return root / brick.slug


def _check_brick_path(brick):
    path = _brick_path(brick)
    assert path.is_dir(), f"{path} is not a valid directory"
    return path


def _build(
    brick,
    *,
    verbose,
    editable,
    isolated,
    build_type,
    editable_rebuild,
    verbose_rebuild,
    no_warnings,
    no_deprecations,
    ignore_deprecations,
    no_index,
    with_subbuilds=True,
):
    path = _check_brick_path(brick)
    _build_banner(brick.name)
    cmd = [str(Path(sys.executable))] + shlex.split("-m pip install")
    if verbose:
        cmd.append("-v")
    if editable:
        cmd.append("-e")
    path_index = len(cmd)
    cmd.append(str(path))
    if not isolated:
        cmd.append("--no-build-isolation")
    if not brick.pure_python:
        cmd.append(f"-Ccmake.build-type={build_type}")
        cmd.append(
            f"-Ceditable.rebuild={'true' if editable_rebuild or verbose_rebuild else 'false'}"
        )
        cmd.append(f"-Ceditable.verbose={'true' if verbose_rebuild else 'false'}")
        if no_warnings:
            cmd.append(f"-Ccmake.define.COMPASS_NO_COMPILATION_WARNINGS=true")
        else:
            cmd.append(f"-Ccmake.define.COMPASS_NO_COMPILATION_WARNINGS=false")
            if ignore_deprecations:
                cmd.append(f"-Ccmake.define.COMPASS_IGNORE_DEPRECATIONS=true")
            else:
                cmd.append(f"-Ccmake.define.COMPASS_IGNORE_DEPRECATIONS=false")
        if no_deprecations:
            cmd.append(("-Ccmake.define.COMPASS_HAS_NO_DEPRECATED_CODE=true"))
    cmd.append(f"-Ccmake.define.CMAKE_BUILD_PARALLEL_LEVEL={os.cpu_count()}")
    click.echo(" ".join(cmd[2:]))
    cmd.append("-Ccmake.minimum-version=3.24")
    if no_index:
        cmd.append("--no-index")
    _run(cmd)
    if with_subbuilds:
        for subpath in brick.subbuilds:
            cmd[path_index] = str(path / subpath)
            _run(cmd)


def _build_api(f):
    @click.option("-v", "--verbose", is_flag=True, help="pip verbose output")
    @click.option("-e", "--editable", is_flag=True, help="pip editable install")
    @click.option(
        "--isolated",
        is_flag=True,
        help="pip isolated build (prefer non isolated build)",
    )
    @click.option(
        "-t",
        "--build-type",
        default="Release",
        help="The build type to use when building the project. Valid options are: Debug, Release, RelWithDebInfo, MinSizeRel...",
    )
    @click.option(
        "--editable-rebuild",
        is_flag=True,
        help="automatically rebuild C++ bindings upon import (will slow down import)",
    )
    @click.option(
        "--verbose-rebuild",
        is_flag=True,
        help="verbose automatic rebuild (implies --editable-rebuild)",
    )
    @click.option(
        "--no-warnings",
        is_flag=True,
        help="do not issue compilation warnings",
    )
    @click.option(
        "--no-deprecations",
        is_flag=True,
        help="remove all deprecations from build",
    )
    @click.option(
        "--ignore-deprecations",
        is_flag=True,
        help="ignore deprecation warnings",
    )
    @click.option(
        "--no-index",
        is_flag=True,
        help="don't use pip index - usefull for offline builds",
    )
    @functools.wraps(f)
    def wrapper(*args, **kwds):
        return f(*args, **kwds)

    return wrapper


def _brick_index(name):
    from compass.bricks import all_bricks

    if name is None:
        return
    try:
        pos = [brick.name for brick in all_bricks].index(name)
    except ValueError:
        print(f"{name} was not recognized as a valid software brick")
        raise
    return pos


def _call_on_brick_selection(f, from_=None, up_to=None, **kwargs):
    from compass.bricks import all_bricks

    begin = _brick_index(from_)
    end = _brick_index(up_to)
    if end is not None:
        end += 1

    for brick in all_bricks[begin:end]:
        f(brick, **kwargs)


@main.command("build")
@click.option(
    "-a",
    "--all",
    is_flag=True,
    help="build all software bricks (takes precedence over all arguments)",
)
@click.option(
    "--from",
    "from_",
    help="build all software bricks from <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--up-to",
    help="build all software bricks up to <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@_build_api
@click.option(
    "--skip-subbuilds",
    is_flag=True,
    help="skip nested builds",
)
@click.argument("brick_names", nargs=-1)
def build_cmd(
    all,
    from_,
    up_to,
    verbose,
    editable,
    isolated,
    build_type,
    editable_rebuild,
    verbose_rebuild,
    no_warnings,
    no_deprecations,
    ignore_deprecations,
    skip_subbuilds,
    no_index,
    brick_names,
):
    """build one or more software bricks"""
    from compass.bricks import get_brick

    call = functools.partial(
        _build,
        verbose=verbose,
        editable=editable,
        isolated=isolated,
        build_type=build_type,
        editable_rebuild=editable_rebuild,
        no_index=no_index,
        verbose_rebuild=verbose_rebuild,
        no_warnings=no_warnings,
        no_deprecations=no_deprecations,
        ignore_deprecations=ignore_deprecations,
        with_subbuilds=not skip_subbuilds,
    )

    if all:
        _call_on_brick_selection(call)
    elif from_ or up_to:
        _call_on_brick_selection(call, from_=from_, up_to=up_to)
    else:
        for name in brick_names:
            call(get_brick(name))


@main.command("doc")
@click.option(
    "-a",
    "--all",
    is_flag=True,
    help="generate documentation for all software bricks (takes precedence over all arguments)",
)
@click.option(
    "--from",
    "from_",
    help="generate documentation from <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--up-to",
    help="generate documentation up to <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--python-only",
    is_flag=True,
    help="generate ony python documentation",
)
@click.option(
    "--cpp-only",
    is_flag=True,
    help="generate ony C++ documentation",
)
@click.argument("brick_names", nargs=-1)
def doc_cmd(
    all,
    from_,
    up_to,
    python_only,
    cpp_only,
    brick_names,
):
    """run pre-commit for one or more software bricks"""

    from compass.bricks import get_brick
    from compass.doc import document_brick

    def call(brick):
        _banner(f"generate documentation for {brick.name}")
        document_brick(brick, python_only=python_only, cpp_only=cpp_only)

    if all:
        _call_on_brick_selection(call)
    elif from_ or up_to:
        _call_on_brick_selection(call, from_=from_, up_to=up_to)
    else:
        for name in brick_names:
            call(get_brick(name))


@main.command("pre-commit")
@click.option(
    "-a",
    "--all",
    is_flag=True,
    help="run pre-commit on all software bricks (takes precedence over all arguments)",
)
@click.option(
    "--from",
    "from_",
    help="run pre-commit from <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--up-to",
    help="run pre-commit up to <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--allow-failure",
    is_flag=True,
    help="allow pre-commit failure",
)
@click.argument("brick_names", nargs=-1)
def pre_commit_cmd(
    all,
    from_,
    up_to,
    allow_failure,
    brick_names,
):
    """run pre-commit for one or more software bricks"""

    from compass.bricks import get_brick

    def call(brick):
        path = _check_brick_path(brick)
        cmd = shlex.split("pre-commit run --all")
        _command_banner(path, cmd)
        _run(cmd, path=path, check=not allow_failure)

    if all:
        _call_on_brick_selection(call)
    elif from_ or up_to:
        _call_on_brick_selection(call, from_=from_, up_to=up_to)
    else:
        for name in brick_names:
            call(get_brick(name))


@main.command("superbuild")
@_build_api
def superbuild_cmd(
    verbose,
    editable,
    isolated,
    build_type,
    editable_rebuild,
    verbose_rebuild,
    no_warnings,
    no_deprecations,
    ignore_deprecations,
):
    """build all software bricks"""
    from compass.bricks import all_bricks

    for brick in all_bricks:
        _build(
            brick,
            verbose=verbose,
            editable=editable,
            isolated=isolated,
            build_type=build_type,
            editable_rebuild=editable_rebuild,
            verbose_rebuild=verbose_rebuild,
            no_warnings=no_warnings,
            no_deprecations=no_deprecations,
            ignore_deprecations=ignore_deprecations,
        )


def _pytest(brick, slow):
    if not brick.has_python_tests:
        return
    path = _check_brick_path(brick)
    cmd = [str(Path(sys.executable))] + shlex.split("-m pytest")
    if slow:
        cmd += shlex.split("-m 'slow or not slow'")
    _run(cmd, path)


@main.command("pytest")
@click.option(
    "-a",
    "--all",
    is_flag=True,
    help="run pytest on all software bricks (takes precedence over all arguments)",
)
@click.option(
    "--from",
    "from_",
    help="run pytest on all software bricks from <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--up-to",
    help="run pytest on software bricks up to <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "-s",
    "--slow",
    is_flag=True,
    show_default=True,
    default=False,
    help="run pytest marked as slow or not slow",
)
@click.argument("brick_names", nargs=-1)
def pytest_cmd(all, from_, up_to, slow, brick_names):
    """run pytest for one or more software bricks"""
    from compass.bricks import get_brick

    if all:
        _call_on_brick_selection(_pytest, slow=slow)
    elif from_ or up_to:
        _call_on_brick_selection(_pytest, from_=from_, up_to=up_to, slow=slow)
    else:
        for name in brick_names:
            _pytest(get_brick(name), slow)


def _cpptest(brick, *, no_deprecations=False):
    if not brick.has_cpp_tests:
        return
    path = _check_brick_path(brick)

    _command_banner(path, cmd=None, message="C++ tests with CTest")

    def run(cmd):
        click.echo(cmd)
        _run(shlex.split(cmd), path)

    builddir = f"_build/{platform.system()}"
    config = f"cmake -B {builddir} -S test -DCMAKE_BUILD_TYPE=Debug"
    if no_deprecations:
        run(f"{config} -DCOMPASS_HAS_NO_DEPRECATED_CODE=ON")
    else:
        run(config)
    run(f"cmake --build {builddir} -j {os.cpu_count()}")
    run(f"ctest --test-dir {builddir} --output-on-failure -C Debug")


@main.command("cpptest")
@click.option(
    "-a",
    "--all",
    is_flag=True,
    help="run C++ test (CTest including catch2) on all software bricks (takes precedence over all arguments)",
)
@click.option(
    "--from",
    "from_",
    help="run C++ test (CTest including catch2) on all software bricks from <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--up-to",
    help="run C++ test (CTest including catch2) on software bricks up to <brick> (takes precendences over independant brick specification)",
    metavar="<brick>",
)
@click.option(
    "--no-deprecations",
    is_flag=True,
    help="remove all deprecations from build",
)
@click.argument("brick_names", nargs=-1)
def cpptest_cmd(all, from_, up_to, no_deprecations, brick_names):
    """run C++ test with CTest (including catch2) for one or more software bricks"""
    from compass.bricks import get_brick

    call = functools.partial(_cpptest, no_deprecations=no_deprecations)

    if all:
        _call_on_brick_selection(call)
    elif from_ or up_to:
        _call_on_brick_selection(call, from_=from_, up_to=up_to)
    else:
        for name in brick_names:
            call(get_brick(name))


def _doctest(brick):
    path = _check_brick_path(brick)

    _command_banner(path, cmd=None, message="C++ tests with doctest")

    def run(cmd):
        click.echo(cmd)
        _run(shlex.split(cmd), path)

    run("cmake -B _build -S test -DCMAKE_BUILD_TYPE=Debug")
    run(f"cmake --build _build -j {os.cpu_count()} --target doctest")
    run("_build/doctest")


@main.command("doctest")
@click.option(
    "-a",
    "--all",
    is_flag=True,
    help="run C++ tests (doctest) on all declared software bricks",
)
@click.argument("brick_names", nargs=-1)
def doctest_cmd(all, brick_names):
    """run C++ test with doctest for one or more software bricks"""
    from compass.bricks import all_bricks, get_brick

    if all:
        for brick in all_bricks:
            _doctest(brick)
    else:
        for name in brick_names:
            _doctest(get_brick(name))


def _banner(message):
    click.echo("")
    click.echo("=" * 80)
    if isinstance(message, str):
        message = [message]
    for line in message:
        click.echo(f"== {line}")
    click.echo("")


def _command_banner(path, cmd="", message=None):
    prefix = ">>"
    lines = [f"{prefix} directory: {path.as_posix()} ".ljust(100, "=")]
    if cmd:
        lines.append(f"{prefix}   command: {' '.join(cmd)}")
    if message:
        if isinstance(message, str):
            lines.append(message)
        else:
            lines.extend(message)
    for line in lines:
        print(line)


def _to_latest_tag(brick, no_checkout=False):
    path = _check_brick_path(brick)
    _command_banner(path)
    cmd = "git describe --tags --abbrev=0 main"
    ret = subprocess.run(shlex.split(cmd), cwd=path, capture_output=True)
    if ret.returncode != 0:
        print(f"WARNING: could not fetch the latest tag for {brick} brick")
        sys.exit(-1)
    tag = ret.stdout.decode().strip()
    print(f"Latest available tag for {brick.name} is: {tag}")
    if no_checkout:
        return
    _run(f"git checkout {tag}".split(), path, check=True)


@main.command("to-latest-tag")
@click.option(
    "-o",
    "--only",
    help="consider a single software brick",
    metavar="<brick>",
    multiple=True,
)
@click.option(
    "-e",
    "--except",
    "exceptions",
    help="does not process a software brick",
    metavar="<brick>",
    multiple=True,
)
@click.option(
    "-n",
    "--no-checkout",
    is_flag=True,
    default=False,
    help="only show tag, don't check out",
)
def to_latest_tag_cmd(only, exceptions, no_checkout):
    """checkout latest available tag (locally on the main branch) for all software bricks

    This supposes that the main branch has been checkout locally and updated.
    """

    from compass.bricks import all_bricks, get_brick

    if only:
        bricks = [get_brick(b) for b in only]
    else:
        bricks = all_bricks
    remove = [get_brick(b) for b in exceptions]
    bricks = [b for b in bricks if not b in remove]

    for b in bricks:
        _to_latest_tag(b, no_checkout)


def _uninstall(brick):
    path = _check_brick_path(brick)
    cmd = [str(Path(sys.executable))] + shlex.split("-m pip uninstall -y")
    cmd.append(brick.name)
    _command_banner(path, cmd)
    _run(cmd, path)


@main.command("uninstall")
@click.option(
    "-a", "--all", is_flag=True, help="uninstall all declared software bricks"
)
@click.argument("brick_names", nargs=-1)
def uninstall_cmd(all, brick_names):
    """uninstall software bricks"""
    from compass.bricks import all_bricks, get_brick

    if all:
        bricks = all_bricks
    else:
        bricks = [get_brick(b) for b in brick_names]

    for b in bricks:
        _uninstall(b)


def _git(brick, *args):
    path = _check_brick_path(brick)
    cmd = ["git"]
    cmd.extend(*args)
    _command_banner(path, cmd)
    # we dont't check return call because git subcommands like grep
    # may return non zero codes if they don't match any pattern
    _run(cmd, path, check=False)


@main.command("git")
@click.option(
    "-o",
    "--only",
    help="run git on a single software brick",
    metavar="<brick>",
)
@click.option(
    "-e",
    "--except",
    "exception",
    help="does not process a software brick",
    metavar="<brick>",
)
@click.argument("args", nargs=-1)
def git_cmd(only, exception, args):
    """run a git command in all software bricks

    NB: to pass git options prepend the full git command line with '--'
    e.g.: compass git -- status -uno
    """

    from compass.bricks import all_bricks, get_brick

    if only and exception:
        click.echo("")
        click.echo("cannot use only and except simultaneously in git command")
        click.echo("")
        return

    if only:
        brick = get_brick(only)
        _git(brick, args)
    else:
        if exception:
            remove = get_brick(exception)
        for brick in all_bricks:
            if exception is None or brick != remove:
                _git(brick, args)


def _shell(brick, *args):
    path = _check_brick_path(brick)
    cmd = list(*args)
    _command_banner(path, cmd)
    _run(cmd, path)


@main.command("shell")
@click.option(
    "-o",
    "--only",
    help="run a shell command in single software brick directory",
    metavar="<brick>",
)
@click.argument("args", nargs=-1)
def shell_cmd(only, args):
    """run a shell command in all software bricks

    NB: to pass options prepend the full command line with '--'
    e.g.: compass shell -- ls -a
    """

    from compass.bricks import all_bricks, get_brick

    if only:
        _shell(get_brick(only), args)
    else:
        for brick in all_bricks:
            _shell(brick, args)


def _clone(repo, brick, remove_existing_path=False):
    path = _brick_path(brick)
    if path.exists():
        click.echo("")
        if remove_existing_path:
            click.echo(f"WARNING: removing {path}")
            click.echo("")

            # from: https://stackoverflow.com/a/2656405
            def onerror(func, path, exc_info):
                if not os.access(path, os.W_OK):
                    os.chmod(path, stat.S_IWUSR)
                    func(path)
                else:
                    raise

            shutil.rmtree(path, onerror=onerror)
        else:
            click.echo(
                f"refusing to clone a repo into an already existing path ({path})"
            )
            click.echo("use --overwrite to replace existing path")
            click.echo("")
            return
    cmd = f"git clone {repo}/{brick.slug} {path.as_posix()}"
    click.echo(cmd)
    _run(shlex.split(cmd))


@main.command("clone")
@click.option(
    "-r",
    "--repo",
    help="compass main git repository",
    default="https://gitlab.com/compass/compass-v5",
    metavar="<repo>",
    show_default=True,
)
@click.option("-a", "--all", is_flag=True, help="clone all declared software bricks")
@click.option("--overwrite", is_flag=True, help="overwrite existing directory")
@click.argument("brick_names", nargs=-1)
def clone_cmd(repo, all, brick_names, overwrite):
    """clone software bricks into compass root"""
    from compass.bricks import all_bricks, get_brick

    if all:
        for brick in all_bricks:
            _clone(repo, brick, remove_existing_path=overwrite)
    else:
        for name in brick_names:
            _clone(repo, get_brick(name), remove_existing_path=overwrite)


@main.command("list-bricks")
@click.option(
    "-s", "--use-slug", "slug", is_flag=True, help="list slugs instead of brick names"
)
@click.option(
    "-i",
    "--use-identifier",
    "identifier",
    is_flag=True,
    help="list identifiers (the string used with python import) instead of brick names",
)
def list_bricks_cmd(slug, identifier):
    """list all software bricks (the default is to list brick names)"""
    from compass.bricks import all_bricks

    if slug and identifier:
        raise click.UsageError(
            "--use-slug and --use-identifier options are mutually exclusive"
        )

    if slug:
        collect = lambda b: b.slug
    elif identifier:
        collect = lambda b: b.identifier
    else:
        collect = lambda b: b.name

    click.echo(" ".join([collect(brick) for brick in all_bricks]))


def _is_brick_dirty(brick, untracked):
    cmd = f"git status --porcelain"
    if not untracked:
        cmd += " -uno"
    path = _check_brick_path(brick)
    ret = subprocess.run(shlex.split(cmd), capture_output=True, cwd=path)
    ret.check_returncode()
    if ret.stdout:
        print(f"{brick.name} is dirty:")
        print(ret.stdout.decode())
        return True
    return False


@main.command("dirty-bricks")
@click.option(
    "-u",
    "--untrack-files",
    "untracked",
    is_flag=True,
    help="consider untracked files too",
)
def dirty_bricks_cmd(untracked):
    """list all software bricks that have uncommited changes"""
    from compass.bricks import all_bricks

    for brick in all_bricks:
        ret = _is_brick_dirty(brick, untracked)


def _first_dirty_brick(untracked):
    from compass.bricks import all_bricks

    for brick in all_bricks:
        if _is_brick_dirty(brick, untracked):
            return brick


def _remove_versioned_conf(name):
    if name in _versioned_configs:
        del _versioned_configs[name]
        _versioned_configs.save()
        click.echo(f"Removed {name} configuration.")
        click.echo(f"You will need to use git commit to actually version your changes.")
        click.echo(f"You can also use git to revert your changes.")
        return True
    return False


def _remove_local_conf(name):
    if name in _local_configs:
        del _local_configs[name]
        _local_configs.save()
        return True
    return False


@main.command("stash")
@click.option(
    "-r",
    "--register",
    is_flag=True,
    help="store configuration in versioned configurations (will need a manual git commit to be actually versioned)",
)
@click.option("-f", "--force", is_flag=True, help="overwrite existing configuration")
@click.option(
    "-u",
    "--untrack-files",
    "untracked",
    is_flag=True,
    help="consider untracked files to determine if bricks are clean",
)
@click.argument("name")
def stash_cmd(register, force, untracked, name):
    """freeze a clean bricks configuration"""
    from compass.bricks import all_bricks

    dirty_brick = _first_dirty_brick(untracked)
    if dirty_brick is not None:
        sys.exit(-1)

    evt = {}
    cmd = f"git rev-parse --verify HEAD"
    for brick in all_bricks:
        path = _check_brick_path(brick)
        ret = subprocess.run(shlex.split(cmd), capture_output=True, cwd=path)
        ret.check_returncode()
        assert not brick.name in evt
        evt[brick.name] = ret.stdout.decode().strip()
    if register:
        if (name not in _versioned_configs) and (name in _local_configs):
            _remove_local_conf(name)
    if (name in _versioned_configs) or (name in _local_configs):
        if not force:
            click.echo(f"configuration {name} already exists!")
            sys.exit(-1)
    conf = _versioned_configs if register else _local_configs
    conf[name] = evt
    conf.save()


@main.command("rm-config")
@click.argument("name")
def rm_config_cmd(name):
    """remove an existing configuration"""
    if not _remove_versioned_conf(name):
        if not _remove_local_conf(name):
            click.echo(f"No configuration named: {name}")
            sys.exit(-1)


def _checkout(brick, sha1, branch_name=None, force=False):
    message = f"checkout {brick.name} to {sha1}"
    cmd = f"git checkout {sha1}"
    if branch_name:
        if force:
            cmd += f" -B {branch_name}"
            message += f" FORCE branch name to {branch_name}"
        else:
            cmd += f" -b {branch_name}"
            message += f" trying to set branch name to {branch_name}"
    click.echo(message)
    path = _check_brick_path(brick)
    ret = subprocess.run(shlex.split(cmd.strip()), capture_output=True, cwd=path)
    if ret.returncode != 0:
        click.echo(ret.stderr)
        sys.exit(-1)


def _latest_versioned():
    wd = parent_dir()
    run = lambda cmd: _run(shlex.split(cmd), wd, capture_output=True, check=False)
    ret = run("git rev-parse --show-toplevel")
    if ret.returncode != 0:
        click.echo(
            f"ERROR: compass does not seem to be installed from it git versioned source directory"
        )
        sys.exit(-1)
    # retrieve the tow latest commit modifying .compass.configurations.versioned
    ret = run(
        "git log --follow --format=format:%H -2 -- .compass.configurations.versioned"
    )
    sha = []
    regexp = re.compile("\A(\S*)$")
    for line in ret.stdout.decode().split("\n"):
        match = regexp.match(line.strip())
        if match:
            sha.append(match.group(1))
    if len(sha) != 2:
        click.echo(
            f"ERROR: could not find at least two commits changing configurations"
        )
        sys.exit(-1)
    ret = run(f"git diff {sha[1]} {sha[0]} -- .compass.configurations.versioned")
    tags = []
    regexp = re.compile("\A(\S*):$")
    for line in ret.stdout.decode().split("\n"):
        if line.startswith("+") and not line.startswith("+++"):
            match = regexp.match(line[1:].rstrip())
            if match:
                tags.append(match.group(1))
    if not tags:
        click.echo(
            f"ERROR: could not find a new configuration with respect to {branch} branch"
        )
        sys.exit(-1)
    if len(tags) > 1:
        click.echo(
            f"ERROR: there are more than a single new configuration with respect to {branch} branch"
        )
        sys.exit(-1)
    return tags[0]


@main.command("latest-versioned")
def latest_versioned():
    click.echo(_latest_versioned())


@main.command("pop")
@click.option(
    "-b",
    "--rename-branch",
    "rename",
    is_flag=True,
    help="give checked-out branch the name of the configuration",
)
@click.option(
    "-f",
    "--force",
    is_flag=True,
    help="rename branch with configuration name even if name already exists",
)
@click.option(
    "-u",
    "--untrack-files",
    "untracked",
    is_flag=True,
    help="consider untracked files to determine if bricks are clean",
)
@click.option(
    "--latest-versioned",
    "latest",
    is_flag=True,
    help="pop the configiration returned by compass latest-versioned (name must be left empty)",
)
@click.argument("name", default="")
def pop_cmd(rename, force, untracked, latest, name):
    """restore a registered bricks configuration"""
    from compass.bricks import get_brick

    if latest:
        if name:
            click.echo(
                f"ERROR: you cannot provide configuration name with --latest-versioned option"
            )
            sys.exit(-1)
        name = _latest_versioned()
    if name in _versioned_configs:
        evt = _versioned_configs[name]
    elif name in _local_configs:
        evt = _local_configs[name]
    else:
        click.echo(f"ERROR: configuration {name} does not exist!")
        sys.exit(-1)

    dirty_brick = _first_dirty_brick(untracked)
    if dirty_brick is not None:
        sys.exit(-1)

    branch_name = None
    if rename or force:
        branch_name = name

    for name, sha1 in evt.items():
        _checkout(get_brick(name), sha1, branch_name, force)


@main.command("ls-configs")
def ls_configs_cmd():
    """list available configurations"""

    def echo(confs, conftype):
        if len(confs) == 0:
            click.echo(f"No {conftype} configurations.")
        else:
            click.echo(f"{conftype.capitalize()} configurations:")
            for name in confs.names():
                click.echo(f"  {name}")

    echo(_versioned_configs, "versioned")
    echo(_local_configs, "local")


def _select_component(patch=True, minor=False, major=False):
    ok = False
    if patch:
        ok = not (minor or major)
        component = "patch"
    elif minor:
        ok = not (patch or major)
        component = "minor"
    else:
        ok = major and not (patch or minor)
        component = "major"
    if not ok:
        click.echo(f"You can select only one of patch, minor or major component.")
        sys.exit(-1)
    return component


def _spread_dependencies_with_tag(
    tag,
    patch,
    minor,
    major,
    dry_run,
    brick,
    local=False,
    tag_changes=False,
    verbose=False,
):
    from compass.bricks import all_bricks, get_brick

    component = _select_component(patch, minor, major)
    begin = _brick_index(brick.name)
    assert begin is not None
    begin += 1
    versions = {brick.name: tag}
    for brick in all_bricks[begin:]:
        gl = GitLabInteractor(_check_brick_path(brick), debug=verbose)
        tag = gl.bump_dependencies(
            versions,
            dry_run=dry_run,
            local=local,
            tag_changes=tag_changes,
            component=component,
        )
        if tag is not None:
            versions[brick.name] = tag
        elif not local:
            versions[brick.name] = gl.latest_tag()


def _spread_dependencies(
    patch, minor, major, dry_run, brick, local=False, tag_changes=False, verbose=False
):
    from compass.bricks import all_bricks, get_brick

    brick = get_brick(brick)
    gl = GitLabInteractor(_check_brick_path(brick), debug=verbose)
    tag = gl.current_tag()
    print(f"{gl.project_name()} is at tag {tag}")
    _spread_dependencies_with_tag(
        tag,
        patch,
        minor,
        major,
        dry_run,
        brick,
        local=local,
        tag_changes=tag_changes,
        verbose=verbose,
    )


@main.command("spread-dependencies")
@click.option(
    "--patch", is_flag=True, default=True, help="bump patch version (default)"
)
@click.option("--minor", is_flag=True, help="bump minor version")
@click.option("--major", is_flag=True, help="bump major version")
@click.option("-d", "--dry-run", is_flag=True, help="only show what would happen")
@click.option(
    "--local",
    is_flag=True,
    help="just modify pyproject.toml file locally, do not push changes nor tags",
)
@click.option(
    "-t", "--tag-changes", is_flag=True, help="create a new tag for every new change"
)
@click.option("-v", "--verbose", is_flag=True, help="show git and glab commands")
@click.argument("brick", type=str, default="")
def spread_dependencies_cmd(
    patch, minor, major, dry_run, local, tag_changes, verbose, brick
):
    _spread_dependencies(
        patch,
        minor,
        major,
        dry_run,
        brick,
        local=local,
        tag_changes=tag_changes,
        verbose=verbose,
    )


@main.command("bump-version")
@click.option("--patch", is_flag=True, help="bump patch version (default)")
@click.option("--minor", is_flag=True, help="bump minor version")
@click.option("--major", is_flag=True, help="bump majpr version")
@click.option("-d", "--dry-run", is_flag=True, help="only show what would happen")
@click.option(
    "-s", "--spread", is_flag=True, help="spread dependencies to downstream bricks"
)
@click.option(
    "--local",
    is_flag=True,
    help="spread dependencies just modify pyproject.toml file locally, do not push changes nor tags",
)
@click.option(
    "-t", "--tag-changes", is_flag=True, help="create a new tag for every new change"
)
@click.option(
    "--follow-changes",
    is_flag=True,
    help="if true propagation will increase the same version component as bumped brick (default is to increase patch version)",
)
@click.option("-m", "--message", type=str, default=None, help="tag annotation")
@click.argument("brick", type=str, default="")
@click.option("-v", "--verbose", is_flag=True, help="show git and glab commands")
def bump_version_cmd(
    patch,
    minor,
    major,
    dry_run,
    spread,
    local,
    tag_changes,
    follow_changes,
    message,
    brick,
    verbose,
):
    """bump version of current directory"""
    from compass.bricks import all_bricks, get_brick

    if not (patch or minor or major):
        patch = True
    component = _select_component(patch, minor, major)
    brick_object = None
    try:
        brick_object = get_brick(brick)
        gl = GitLabInteractor(_check_brick_path(brick_object), debug=verbose)
    except ValueError:
        gl = GitLabInteractor(debug=verbose)
    if local and not dry_run:
        gl.merge_into_main()
    new_tag = gl.next_tag(component)
    gl.tag_main(new_tag, annotation=message, dry_run=dry_run)
    if brick_object is not None and spread:
        if not follow_changes:
            patch = True
            minor = major = False
        _spread_dependencies_with_tag(
            new_tag,
            patch,
            minor,
            major,
            dry_run,
            brick_object,
            local=local,
            tag_changes=tag_changes,
        )
    else:
        if spread:
            click.echo("I will not spread dependencies of an unknwon software brick.")
        else:
            if local:
                click.echo("--local has no effect without --spread")
            if tag_changes:
                click.echo("--tag-changes has no effect without --spread")


@main.command("change-gitlab-token")
@click.option("-u", "--user", type=str, default="__token__", help="the user id")
@click.option("-t", "--token", type=str, default="", help="the token")
@click.option(
    "-r", "--remote", type=str, default="origin", help="the remote repository"
)
@click.option("--git-suffix", is_flag=True, help="add .git suffix to url if missing")
def change_gitlab_token_cmd(user, token, remote, git_suffix):
    """change remote identification token on all bricks"""

    def f(brick):
        path = _brick_path(brick)
        cmd = f"git remote get-url {remote}"
        ret = _run(shlex.split(cmd), path, capture_output=True)
        url = ret.stdout.decode().strip()
        assert url.startswith("https://")
        parts = re.match("https://(.*\@)(.*)", url)
        if parts is None:
            parts = re.match("https://(.*)", url)
        sep = ":" if token else ""
        new_url = f"https://{user}{sep}{token}@{parts.groups()[-1]}"
        if git_suffix and not url.endswith(".git"):
            new_url += ".git"
        cmd = f"git remote set-url {remote} {new_url}"
        ret = _run(shlex.split(cmd), path)

    _call_on_brick_selection(f)
