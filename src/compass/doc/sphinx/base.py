# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

import sys
import os
import sphinx_rtd_theme

from pathlib import Path

# -- ComPASS specific --------------------------------------------------------

assets = Path(__file__).parent / "assets"

assert assets.is_dir()
assert assets.exists()

asset = lambda name: (assets / name).as_posix()

compass_logo = asset("logo.png")

# -- Project information -----------------------------------------------------

root_doc = "index"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autosummary",
    "sphinx.ext.ifconfig",
    "recommonmark",  # for MarkDown
    "sphinx_tabs.tabs",
]

templates_path = ["_templates"]

exclude_patterns = [
    "CMakeLists.txt",
    "adr/*",
    "_build",
    "Thumbs.db",
    ".DS_Store",
    "README.txt",
]

source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

rst_epilog = """
.. include:: <s5defs.txt>
.. raw:: html

    <style> .red {color:red} </style>
    <style> .lime {color:lime} </style>
"""
