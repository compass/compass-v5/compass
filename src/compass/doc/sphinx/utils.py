import re
from setuptools_scm import get_version
from pathlib import Path


def get_version_info(path):

    path = Path(path)
    if path.is_file():
        assert path.name == "conf.py"
        assert path.parent.name == "doc"
        path = path.parent.parent
    assert path.is_dir() and path.exists()

    scm_version = get_version(root=path)
    matches = re.match("(\d+\.\d+.\d+)(.*)", scm_version)
    version, tag = matches.groups()
    if len(tag) > 0:
        assert tag.startswith(".dev")
        version = f"{version}-dev"
    return version, scm_version
