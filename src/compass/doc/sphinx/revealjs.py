from compass.doc.sphinx.base import *

# -- Options for reveal.js output --------------------------------------------
# sphinx-build -M revealjs . .
revealjs_style_theme = "beige"
# contains static path (to store images or style files)
revealjs_static_path = ["_static"]

revealjs_script_conf = {
    "controls": True,
    "progress": True,
    "center": True,
    "transition": "slide",
}
revealjs_script_plugins = [
    {
        "name": "RevealNotes",
        "src": "revealjs/plugin/notes/notes.js",
    },
    {
        "name": "RevealHighlight",
        "src": "revealjs/plugin/highlight/highlight.js",
    },
]
revealjs_css_files = [
    "revealjs/plugin/highlight/zenburn.css",
]

revealjs_exclude_patterns = [
    "revealjs",
    "training/revealjs",
    "training/conf.py",
    "training/test",
    "training/meshes",
]

try:
    exclude_patterns.extend(revealjs_exclude_patterns)
except NameError:
    exclude_patterns = revealjs_exclude_patterns
