from pathlib import Path
import sys
import shlex
import subprocess
from string import Template

templates_directory = Path(__file__).parent / "templates"


def generate(replacements, *args, no_cpp=False):
    replacements["cpp_api"] = "" if no_cpp else "C++ API <./cpp/index.html#https://>"
    for path in args:
        assert not path.is_file()
        template = templates_directory / f"{path.name}.in"
        assert template.is_file()
        with path.open("w") as f:
            with template.open("r") as g:
                for line in g:
                    f.write(Template(line).substitute(replacements))


def make(docdir):
    assert docdir.is_dir()
    subprocess.run(
        [sys.executable] + shlex.split(f"-m sphinx.cmd.build {docdir.as_posix()} html"),
        cwd=docdir,
    )
