from compass.doc.sphinx.base import *

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_rtd_theme"
# contains static path (to store images or style files)
# html_static_path = ["_static"]
html_css_files = ["custom.css"]  # todo to improve Solution appearance
# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = compass_logo

# Add a timestamp using datetime format
html_last_updated_fmt = "%b %d, %Y, %X"
