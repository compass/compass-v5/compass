from compass.doc.sphinx.base import *

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass [howto/manual]).
# sphinx-build -b latex . ./latex/; cd latex; make; cd -
latex_documents = [
    (
        "training/latex_index",
        "training_ComPASS_initiation.tex",
        "ComPASS Initiation",
        "L. Beaude, S. Lopez, F. Smai and various contributors",
        "manual",
    ),
]
latex_logo = compass_logo
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    "papersize": "a4paper",
    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',
    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',
    # Latex figure (float) alignment
    #
    "figure_align": "!htb",
    "extraclassoptions": "openany",
}
