from compass import _check_brick_path
import compass.doc.doxygen as doxygen
import compass.doc.sphinx as sphinx
from pathlib import Path
from tempfile import NamedTemporaryFile


class _DocInfo:
    def __init__(self, brick):
        self.brick = brick
        self.brick_path = _check_brick_path(brick)
        assert self.brick_path.is_dir()
        self.doc_path = self.brick_path / "doc"
        self.html_path = self.doc_path / "html"
        self.html_path.mkdir(parents=True, exist_ok=True)
        if not brick.pure_python:
            self.doxygen_path = self.doc_path / "doxygen"
            (self.doxygen_path / "images").mkdir(parents=True, exist_ok=True)
            if brick.pure_cpp:
                self.html_cpp_path = self.html_path
            else:
                self.html_cpp_path = self.html_path / "cpp"
                self.html_cpp_path.mkdir(exist_ok=True)


def make_doxygen(docinfo):
    doxydir = docinfo.doxygen_path
    custom_config = doxydir / "Doxyfile.in"
    # FIXME: with python>=3.12 use delete_on_close option and close the file before running doxygen
    with NamedTemporaryFile("w", delete=False) as f:
        input_path = docinfo.brick_path / "src" / docinfo.brick.identifier
        assert input_path.is_dir(), f"{input_path.as_posix()} is not a directory"
        example_path = docinfo.brick_path / "test"
        assert example_path.is_dir(), f"{example_path.as_posix()} is not a directory"

        def relative(path):
            # FIXME: walk_up comes with python>=3.12
            # return path.relative_to(doxydir, walk_up=True).as_posix()
            assert (doxydir / "../..").resolve() == docinfo.brick_path.resolve()
            return Path("../..") / path.relative_to(docinfo.brick_path).as_posix()

        print(f"INPUT = . {relative(input_path)}", file=f)
        print(f"PROJECT_NAME = {docinfo.brick.name}", file=f)
        print(f"EXAMPLE_PATH = {relative(example_path)}", file=f)
        print(f"HTML_OUTPUT = {relative(docinfo.html_cpp_path)}", file=f)
        if custom_config.is_file():
            with custom_config.open("r") as g:
                for line in g:
                    f.write(line)
        config_file = Path(f.name)
    doxygen.make(doxydir, config_file)
    config_file.unlink()


def make_sphinx(docinfo):
    docdir = docinfo.doc_path
    brick = docinfo.brick
    replacements = {"project_name": brick.name, "module_name": brick.identifier}
    generated = []

    def check(path):
        if not path.is_file():
            generated.append(path)

    check(docdir / "conf.py")
    check(docdir / "index.rst")
    check(docdir / "python_api.rst")
    sphinx.generate(replacements, *generated, no_cpp=brick.pure_python)
    sphinx.make(docdir)


def make(docinfo, *, with_sphinx=True, with_doxygen=True):
    if with_doxygen:
        make_doxygen(docinfo)
    if with_sphinx:
        make_sphinx(docinfo)


def document_brick(brick, *, python_only=False, cpp_only=False):
    cpp_only = cpp_only or brick.pure_cpp
    python_only = python_only or brick.pure_python
    docinfo = _DocInfo(brick)
    make(docinfo, with_sphinx=not cpp_only, with_doxygen=not python_only)
