from pathlib import Path
import subprocess
from tempfile import NamedTemporaryFile


module_directory = Path(__file__).parent


def make(doxydir, custom_config=None):
    assert doxydir.is_dir()
    if custom_config is None:
        custom_config = doxydir / "Doxyfile.in"
    assert custom_config.is_file()
    # FIXME: with python>=3.12 use delete_on_close option and close the file before running doxygen
    with NamedTemporaryFile("w", delete=False) as f:

        def _append(path):
            assert path.is_file()
            with path.open("r") as g:
                for line in g:
                    f.write(line)

        base_config = module_directory / "Doxyfile"
        _append(base_config)
        header = module_directory / "header.html"
        assert header.is_file()
        print(f"HTML_HEADER            = {header.as_posix()}", file=f)
        footer = module_directory / "footer.html"
        assert footer.is_file()
        print(f"HTML_FOOTER            = {footer.as_posix()}", file=f)
        _append(custom_config)
        config = Path(f.name)
    assert config.is_file()
    subprocess.run(["doxygen", config.as_posix()], cwd=doxydir)
    config.unlink()
