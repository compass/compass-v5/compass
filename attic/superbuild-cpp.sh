#!/bin/bash

set -e

script_dir=`realpath $( dirname -- "${0}" )`
if [ -z "${1}" ]
then
  superbuild_dir="${script_dir}/build"
else
  superbuild_dir="${script_dir}/${1}"
fi
if [ -z "${CI_REPOSITORY_URL}" ]
then
  compass_default_git_root="https://gitlab.brgm.fr/brgm/modelisation-geologique/compass"
else
  compass_default_git_root="${CI_REPOSITORY_URL%/*}"
fi

# With adress sanitizer:
# CXXFLAGS="-g -O0 -fsanitize=address" cmake -B ${superbuild_dir} -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=ON -DBUILD_DOC=OFF -DCOMPASS_DEFAULT_GIT_ROOT=${compass_default_git_root}
cmake -B ${superbuild_dir} -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=ON -DBUILD_DOC=OFF -DCOMPASS_DEFAULT_GIT_ROOT=${compass_default_git_root}
# cmake -B ${superbuild_dir} -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DBUILD_TESTING=ON -DBUILD_DOC=OFF -DCOMPASS_DEFAULT_GIT_ROOT=${compass_default_git_root}
cmake --build ${superbuild_dir} -j 8
