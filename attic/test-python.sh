#!/bin/bash

script_dir=`realpath $( dirname -- "${0}" )`

source ${script_dir}/python_modules_list

# collect all test

for module in ${compiled_modules}
do
  testdir="${script_dir}/modules/${module}/python/test"
  if [ -d ${testdir} ]
  then
    if [ -z test_directories ]
    then
      test_directories="${testdir}"
    else
      test_directories="${test_directories} ${testdir}"
    fi
  fi
done

for module in ${pure_modules}
do
  testdir="${script_dir}/modules/${module}/test"
  if [ -d ${testdir} ]
  then
    if [ -z test_directories ]
    then
      test_directories="${testdir}"
    else
      test_directories="${test_directories} ${testdir}"
    fi
  fi
done

# LD_PRELOAD=/home/simon/miniconda3/envs/sdk-3.10/envs/compass/lib/libasan.so.6  python -m pytest ${test_directories}
python -m pytest ${test_directories}
