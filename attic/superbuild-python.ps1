$script_dir = Split-Path -Parent $MyInvocation.MyCommand.Path

. ${script_dir}\python_modules_list.ps1

foreach ($module in $compiled_modules) {
    if (-not $args) {
        & "$script_dir/build-python-module.ps1" $module "build"
    }
    else {
        & "$script_dir/build-python-module.ps1" $module $args[0]
    }
}

foreach ($module in $pure_modules) {
    python -m pip install -e "modules/$module" --no-build-isolation
}
