There is an easier way to do this as well, if you prefer to not manually fetch and merge in the subdirectory. If you run git submodule update --remote, Git will go into your submodules and fetch and update for you.

git config -f .gitmodules submodule.DbConnector.branch stable

gs foreach "git push -d origin spbuild-rename-tests || true"


set-branch (-b|--branch) <branch> [--] <path>
set-branch (-d|--default) [--] <path>

    Sets the default remote tracking branch for the submodule. The --branch option allows the remote branch to be specified. The --default option removes the submodule.<name>.branch configuration key, which causes the tracking branch to default to the remote HEAD.



git describe uses only annotated tags by default.


git submodule foreach git checkout origin/HEAD^0


git submodule foreach git diff --quiet HEAD || git commit -a -m "Rename python tests for superbuild"
