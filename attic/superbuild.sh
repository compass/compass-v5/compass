#!/bin/bash

set -e

script_dir=`realpath $( dirname -- "${0}" )`

/bin/bash ${script_dir}/superbuild-cpp.sh ${1}
/bin/bash ${script_dir}/superbuild-python.sh ${1}
