$currentDir = Get-Location
$scriptDir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$submoduleDir = Join-Path $scriptDir "modules\$($args[0])"

if (-not $args[1]) {
    $superbuildDir = Join-Path $scriptDir "build"
} else {
    $superbuildDir = Join-Path $scriptDir $args[1]
}

Write-Host "Building $($args[0]) python module"
Write-Host "submodule directory: $submoduleDir"
Write-Host "superbuild directory: $superbuildDir"

if (-not $env:CI_REPOSITORY_URL) {
    $compassDefaultGitRoot = "https://gitlab.brgm.fr/brgm/modelisation-geologique/compass"
} else {
    $compassDefaultGitRoot = $env:CI_REPOSITORY_URL -replace "/[^/]*$"
}

#$cmakeConfigureOptions = "-DCOMPASS_SUPERBUILD=ON -DCMAKE_PREFIX_PATH=${superbuildDir}\cmake -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF"
$skbuildType = "Debug"
Set-Location $submoduleDir\python
python setup.py develop -G "Visual Studio 17 2022" --build-type $skbuildType -DCOMPASS_SUPERBUILD=ON -DCMAKE_PREFIX_PATH="${superbuildDir}\cmake" -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF

if ($args[0] -eq "icus") {
   Set-Location $submoduleDir\python\test\custom_field_example
   python setup.py develop -G "Visual Studio 17 2022" --build-type $skbuildType -DCOMPASS_SUPERBUILD=ON -DCMAKE_PREFIX_PATH="${superbuildDir}\cmake" -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF
}

Set-Location $currentDir
