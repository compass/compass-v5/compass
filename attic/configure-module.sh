#!/bin/bash

set -e

script_dir=`realpath $( dirname -- "${0}" )`
submodule_dir="${script_dir}/modules/${1}"
if [ -z "${2}" ]
then
  superbuild_dir="${script_dir}/build"
  build_dir="${submodule_dir}/build"
else
  superbuild_dir="${script_dir}/${2}"
  build_dir="${submodule_dir}/${2}"
fi

ls ${superbuild_dir}/cmake

cmake -S ${submodule_dir} -B ${build_dir} -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=ON -DBUILD_DOC=OFF -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF -DCOMPASS_SUPERBUILD=ON -DCMAKE_PREFIX_PATH=${superbuild_dir}/cmake
