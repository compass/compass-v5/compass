$script_dir = Split-Path -Parent $MyInvocation.MyCommand.Path

. ${script_dir}\python_modules_list.ps1

$test_directories = @()

foreach ($module in $compiled_modules) {
    $testdir = "$script_dir/modules/$module/python/test"
    if (Test-Path $testdir -PathType Container) {
        $test_directories += $testdir
    }
}

foreach ($module in $pure_modules) {
    $testdir = "$script_dir/modules/$module/test"
    if (Test-Path $testdir -PathType Container) {
        $test_directories += $testdir
    }
}

python -m pytest $test_directories
