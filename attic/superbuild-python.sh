#!/bin/bash

set -e

script_dir=`realpath $( dirname -- "${0}" )`

source ${script_dir}/python_modules_list

for module in ${compiled_modules}
do
  if [ -z "${1}" ]
  then
    /bin/bash ${script_dir}/build-python-module.sh ${module} build
  else
    /bin/bash ${script_dir}/build-python-module.sh ${module} ${1}
  fi
done

for module in ${pure_modules}
do
  python -m pip install -e modules/${module} --no-build-isolation
done
