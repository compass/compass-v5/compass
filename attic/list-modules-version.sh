#!/bin/bash

script_dir=`realpath $( dirname -- "${0}" )`

for d in ${script_dir}/modules/*
do
  if [ -d $d ]
  then
    version=`(cd $d && git describe --tags)`
    branch=`(cd $d && git rev-parse --abbrev-ref HEAD)`
    status=`(cd $d && git status -uno -s)`
    if [[ ! -z ${status##*( )} ]]
    then
      echo "> ${d##*/}: ${version} (${branch})"
      echo ${status}
    else
      echo "= ${d##*/}: ${version} (${branch})"
    fi
  fi
done
