#!/bin/bash

script_dir=`realpath $( dirname -- "${0}" )`

cwd=$(pwd)

for d in ${script_dir}/modules/*
do
if [ -d $d ]
then
  cd ${d}
  git diff --quiet HEAD || git commit "${@}"
fi
done

cd ${cwd}
