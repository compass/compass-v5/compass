$compiled_modules = @("icus", "icmesh", "geom-traits", "loaf", "scheme-lfv", "physics", "physicalprop", "coats-variables", "compass-coats")
$pure_modules = @("compass-python-utils", "physical-properties-library", "globalgo")
