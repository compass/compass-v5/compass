#!/bin/bash

script_dir=`realpath $( dirname -- "${0}" )`
cwd=$(pwd)

# we checkout first to origin/HEAD so that
# git describe actually finds the latest tag
cd ${script_dir}
git submodule foreach git checkout origin/HEAD^0
git submodule update --remote

for d in ${script_dir}/modules/*
do
  if [ -d $d ]
  then
    cd ${d}
    tag=`git describe --tags --abbrev=0`
    git checkout ${tag}
  fi
done

cd ${cwd}
