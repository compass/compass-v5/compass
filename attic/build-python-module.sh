#!/bin/bash

set -e

current_dir=$(pwd)

script_dir=`realpath $( dirname -- "${0}" )`
submodule_dir="${script_dir}/modules/${1}"
if [ -z "${2}" ]
then
  superbuild_dir="${script_dir}/build"
else
  superbuild_dir="${script_dir}/${2}"
fi

echo "Building $1 python module"
echo "submodule directory: ${submodule_dir}"
echo "superbuild directory: ${superbuild_dir}"

if [ -z "${CI_REPOSITORY_URL}" ]
then
  compass_default_git_root="https://gitlab.brgm.fr/brgm/modelisation-geologique/compass"
else
  compass_default_git_root="${CI_REPOSITORY_URL%/*}"
fi
# Cmake option to work disconnected: -DFETCHCONTENT_FULLY_DISCONNECTED=ON
# CMAKE_CONFIGURE_OPTIONS="-DFETCHCONTENT_FULLY_DISCONNECTED=ON -DCOMPASS_SUPERBUILD=ON -DCMAKE_PREFIX_PATH=${superbuild_dir}/cmake -DCOMPASS_DEFAULT_GIT_ROOT=${compass_default_git_root} -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF"
CMAKE_CONFIGURE_OPTIONS="-DCOMPASS_SUPERBUILD=ON -DCMAKE_PREFIX_PATH=${superbuild_dir}/cmake -DCOMPASS_DEFAULT_GIT_ROOT=${compass_default_git_root} -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF"
SKBUILD_BUILD_OPTIONS="-j $(nproc) --build-type Debug"
# for py-spy
# SKBUILD_BUILD_OPTIONS="-j $(nproc) --build-type RelWithDebInfo"
SKBUILD_OPTIONS="-GUnix Makefiles"
# python -m pip install --no-build-isolation ${submodule_dir}/python
# FIXME: pip and scikit-build do not work in editable mode
# python -m pip install -vvv -e ${submodule_dir}/python --no-build-isolation
cd ${submodule_dir}/python
# With adress-sanitizer:
# CXXFLAGS="-g -O0 -fsanitize=address" python setup.py develop "${SKBUILD_OPTIONS}" ${SKBUILD_BUILD_OPTIONS} ${CMAKE_CONFIGURE_OPTIONS}
python setup.py develop "${SKBUILD_OPTIONS}" ${SKBUILD_BUILD_OPTIONS} ${CMAKE_CONFIGURE_OPTIONS}
if [ ${1} = "icus" ]
then
  cd ${submodule_dir}/python/test/custom_field_example
  # CXXFLAGS="-g -O0 -fsanitize=address" python setup.py develop "${SKBUILD_OPTIONS}" ${SKBUILD_BUILD_OPTIONS} ${CMAKE_CONFIGURE_OPTIONS}
  python setup.py develop "${SKBUILD_OPTIONS}" ${SKBUILD_BUILD_OPTIONS} ${CMAKE_CONFIGURE_OPTIONS}
fi

cd ${current_dir}
