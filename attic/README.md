# ComPASS superbuild

Make a **recursive** clone to include all submodules.

```shell
git clone --recurse-submodules https://gitlab.brgm.fr/brgm/modelisation-geologique/compass/compass.git
```

Then the `superbuild.sh` script should handle things for you just running

```shell
/bin/bash superbuild.sh
```

# Developing a single module

## Just use the superbuild tree

Suppose that SBD holds the path to your *super* build tree
(it will default to `path_to_compass/build`).

Then just doing:

```shell
make -C ${SBD}/modules/icus -j 8
```

you will recompile **only** the icus module.

And to run all icus tests just do:

```shell
make -C ${SBD}/modules/icus -j 8 test
```

### Use a specific module branch

If you want to use a specific module branch to test your work
(locally or on the CI) just add your branch name
in the `.gitmodules` file section corresponding to your module:

e.g. working with the `debug-superbuild` branch of the loaf project:

```shell
[submodule "modules/loaf"]
	path = modules/loaf
	url = https://gitlab.brgm.fr/brgm/modelisation-geologique/compass/loaf.git
	branch = debug-superbuild
```



# Building and testing all python modules

The `superbuild-python.sh` will build all python packages **in development mode**.
The `test-python.sh` will test all python packages using pytest.

## Developing a single module

As the modules are installed in development mode all changes in your python
scripts will be immediately taken into account.

If there is changes on the C++ side you can recompile using the command:

```
make -C path_to_compass/modules/icus/_skbuild/your_config/cmake-build -j 8
```

If changes affect several module prefer to use the `superbuild-python.sh` script
to check all dependencies.

# To go further

## The details of surperbuild

The project is configured using CMake:

```shell
cmake -B build/linux -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=ON -DBUILD_DOC=OFF
```

It can be build with:
```shell
cmake --build build/linux -j 8
```

Note that `cmake --build` will call the chosen build tool under the hood.
So if you're working with cmake, it can be replaced with:
```shell
make -C build/linux -j 8
```

You can run all C++ test with either:
```shell
cmake --build build/linux --target test -j 8
```

or:
```shell
make -C build/linux -j 8 test
```

### If you want to build a project in a separate directory

The script `configure-submodule.sh` will give you hints about how
to compile a specific module using dependencies from the superbuild directory.

## To work fully disconnected

You can set the cmake variable `FETCHCONTENT_FULLY_DISCONNECTED`
just adding the line:

```cmake
 set(FETCHCONTENT_FULLY_DISCONNECTED ON)
```

in the main `CMakeLists.txt`.


## To compile with address sanitizer

You can set the environment variable :
`CXXFLAGS="-g -O0 -fsanitize=address"`

To run a python script with asan you must preload the shared library.

Use ldd to check the name of the shared library then do (e.g.) :

`LD_PRELOAD=/home/simon/miniconda3/envs/sdk-3.10/envs/compass/lib/libasan.so.6 python my_script.py`
