#!/bin/bash

script_dir=`realpath $( dirname -- "${0}" )`

for d in ${script_dir}/modules/*
do
  if [ -d $d ]
  then
    # git config -f .gitmodules submodule.${d##*/}.branch main
    (cd ${script_dir} && git submodule set-branch -b main modules/${d##*/})
  fi
done
