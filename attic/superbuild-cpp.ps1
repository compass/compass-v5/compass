$scriptDir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$superbuild_dir = Join-Path $scriptDir "build\windows"
$compass_default_git_root = "https://gitlab.brgm.fr/brgm/modelisation-geologique/compass"

cmake -B $superbuild_dir -G "Visual Studio 17 2022" -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=ON -DBUILD_DOC=OFF -DCOMPASS_DEFAULT_GIT_ROOT="${compass_default_git_root}"
cmake --build $superbuild_dir --config Debug -j 8
